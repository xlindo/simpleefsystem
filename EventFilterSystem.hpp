#include <iostream>
#include <vector>
#include <string>
#include <any>
#include <functional>
#include <thread>
#include <future>
#include <cstdint>

struct Event
{
    uint64_t id = 0;
    uint32_t timestamp = 0;
};

struct EventFilterSystem
{
    std::vector<std::function<std::any(std::any, std::any)>> _fns;
    std::vector<std::any> _args;

    template <typename R, typename A, typename B>
    void add(R (*f)(A, B), B arg)
    {
        _fns.emplace_back([f](std::any x, std::any y) -> std::any {
            return {f(std::any_cast<A>(x), std::any_cast<B>(y))};
        });
        _args.push_back(std::any_cast<B>(arg));
    }

    template <typename T>
    std::any run_rec(T x, const std::size_t next)
    {
        return next == _fns.size() - 1
                   ? _fns[next](x, _args[next])
                   : run_rec(_fns[next](x, _args[next]), next + 1);
    }

    template <typename T>
    std::any run(T x)
    {
        return run_rec(x, 0);
    }

    template <typename T>
    std::any run_async(T x)
    {
        std::future<T> ef_t = std::async(run_rec, x, 0);
        // return ef_t.get();
    }
};