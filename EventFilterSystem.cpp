/**
 * @file EventFilterSystem.cpp
 * @author Xinlin DU (hi@xlindo.com)
 * @brief This file is for the test of EventFilterSystem. More details are in "EventFilterSystem.hpp".
 * With this system, you can perform your desired filtering processes in order.
 * 
 * The compilation command could be
 *  g++ EventFilterSystem.cpp -o EventFilterSystem -std=c++17
 * 
 * Usage:
 *  1. Arrange your functions, either function names or args
 *  2.  """
 *      vector<Event> orig_events{init};
 *      EventFilterSystem EFS;
 * 
 *      EFS.add(func_1, arg_1);
 *      EFS.add(func_2, arg_2);
 * 
 *      auto res_events = any_cast<vector<Event>>(EFS.run(orig_events));
 *      """
 *  
 * TODO:
 *  1. Multi-threading. Since there is a recursive process, so running in threads may be not a good idea, this requirement should be considered furthur. Some old implementation could be seen in https://gitlab.com/xlindo/simpleefsystem/-/blob/master/archived/EventFilterProxyMain.cpp
 *  2. Pass by value or pass by reference. This is not considered clearly, since I am not familier with std::any now.
 *  3. No exception process is considered now
 * 
 * 
 * @version 0.1
 * @date 2020-05-14
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <string>
#include <thread>
#include <algorithm>

#include "EventFilterSystem.hpp"

using namespace std;

void print_thread_id(const string &func_name)
{
  std::thread::id tid = std::this_thread::get_id();
  std::cout << "Function " << func_name << " runs in thread_id= " << tid << std::endl
            << std::endl;
}

void print_contents(const vector<Event> &events)
{
  cout << "========= Contents =========" << endl;
  for (const auto &evt : events)
  {
    cout << evt.id << " " << evt.timestamp << endl;
  }
  cout << "========= End =========" << endl
       << endl;
}

vector<Event> remove_id_n(vector<Event> events, vector<uint64_t> id_rm)
{

  print_thread_id(__FUNCTION__);

  vector<Event> res_events;

  auto iter = events.cbegin();
  while (iter != events.cend())
  {
    if (id_rm.cend() == find(id_rm.cbegin(), id_rm.cend(), iter->id))
    {
      res_events.push_back(*iter);
    }
    ++iter;
  }

  return res_events;
}

vector<Event> add_event(vector<Event> events, vector<Event> new_events)
{
  print_thread_id(__FUNCTION__);

  vector<Event> res_events(events);
  res_events.insert(res_events.end(), new_events.begin(), new_events.end());

  return res_events;
}

int main()
{
  vector<Event> three_evts, two_evts;
  Event evt_1, evt_2, evt_3, evt_4, evt_5;
  evt_1.id = 2;
  evt_1.timestamp = 5070001;
  evt_2.id = 2;
  evt_2.timestamp = 5100002;
  evt_3.id = 3;
  evt_3.timestamp = 5100003;
  three_evts.push_back(evt_1);
  three_evts.push_back(evt_2);
  three_evts.push_back(evt_3);

  evt_4.id = 4;
  evt_4.timestamp = 5100004;
  evt_5.id = 5;
  evt_5.timestamp = 5100005;
  two_evts.push_back(evt_4);
  two_evts.push_back(evt_5);

  // Test: {1,2,3} --rm 1--> {2,3} --rm 2--> {3} --add 4,5--> {3,4,5}
  EventFilterSystem EFS;

  vector<uint64_t> v_u64_1{1}, v_u64_2{2};

  EFS.add(remove_id_n, v_u64_1);
  EFS.add(remove_id_n, v_u64_2);
  EFS.add(add_event, two_evts);

  EventFilterSystem EFS_async;

  EFS_async.add(remove_id_n, v_u64_1);
  EFS_async.add(remove_id_n, v_u64_2);
  EFS_async.add(add_event, two_evts);

  auto res_events = any_cast<vector<Event>>(EFS.run(three_evts));
  auto res_events_async = any_cast<vector<Event>>(EFS_async.run_async(three_evts));

  print_contents(res_events);
  print_contents(res_events_async);

  return 0;
}