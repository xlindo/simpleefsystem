/**
 * @file main.cpp
 * @author Xinlin DU (hi@xlindo.com)
 * @brief This is an event filtering system including some simple functions. The main idea is hidden in the class (EventFilter.h). It can
 * 1. Transform event IDs and timestamps.
 * 2. Add events to the collection.
 * 3. Remove events to the collection based on a predicate.
 * 4. Filter the events with determined ID in main thread or in separate thread
 * 5. Simple test or user guide is in main.cpp
 * 
 * Some other important issues should also be pointed out here:
 * 1. No exception processing is set yet
 * 2. No locker is set yet, since the input is assumed correct
 * 3. Debug operations are not removed
 * 
 * The compilation command could be 
 *  
 *  g++ main.cpp EventFilter.h EventFilter.cpp -o EventFilter -std=c++11 -pthread
 *
 * 
 * @version 0.1
 * @date 2020-05-10
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <thread>
#include <future>
#include "EventFilter.h"

void print_contents(const std::vector<Event> &events)
{
    std::cout << "========= Events Inside ==========" << std::endl;
    for (const auto &evt : events)
    {
        std::cout << evt.id << " " << evt.timestamp << std::endl;
    }
    std::cout << std::endl;
}

int main()
{
    // Test 1
    // 1.1 Init an event filter with all events
    Event single_event;
    single_event.id = 1;
    single_event.timestamp = 5100015;

    auto ef = new EventFilter(single_event);

    // 1.2 add an events
    ef->add_events(12, 5100015);
    print_contents(ef->get_m_events());

    // 1.3 remove a collection of events based on id
    ef->remove_events(12);
    print_contents(ef->get_m_events());

    // 1.4 add 2 events with same id
    ef->clear();
    print_contents(ef->get_m_events());

    std::vector<Event> two_evts;
    Event evt_1, evt_2;
    evt_1.id = 12;
    evt_1.timestamp = 5070001;
    evt_2.id = 12;
    evt_2.timestamp = 5100002;
    two_evts.push_back(evt_1);
    two_evts.push_back(evt_2);

    ef->add_events(two_evts);
    print_contents(ef->get_m_events());

    // 1.5 remove a collection of events based on id
    ef->remove_events(12);
    print_contents(ef->get_m_events());

    // 1.6 return a queue of events with a determined id (and time range)
    ef->clear();

    ef->add_events(two_evts);
    ef->add_events(1, 5071112);
    std::vector<Event> evts_12 = ef->event_filter(ef->get_m_events(), 12);
    print_contents(evts_12);

    // 1.7 process in chain

    std::vector<Event> evts_1212 = ef->event_filter(ef->add_events(ef->event_filter(ef->get_m_events(), 12)), 12);
    print_contents(evts_1212);

    // 1.8 Process in another filter and another thread
    auto ef_ts = new EventFilter(evts_12);
    ef_ts->add_events(1, 5071112);
    std::vector<Event> evts_12_thread = ef->event_filter_thread(ef->get_m_events(), 1);
    print_contents(evts_12_thread);


    delete ef;
    delete ef_ts;
    ef = nullptr;
    ef_ts = nullptr;

    return 0;
}
