//
// Created by xlindo on 2020/5/9.
//

#include "EventFilter.h"

EventFilter::EventFilter(const Event &evt)
{
	m_events = {evt};
}

EventFilter::EventFilter(const std::vector<Event> &events) : m_events(events)
{
}

EventFilter::EventFilter(std::queue<Event> events)
{
	while (!events.empty())
	{
		m_events.push_back(events.front());
		events.pop();
	}
}

/**
 * @brief Modify the target event to new records
 * 
 * @return std::vector<Event> 
 */
std::vector<Event>
EventFilter::transform_event(const uint64_t &old_id, const uint32_t &old_timestamp, const uint64_t &new_id, const uint32_t &new_timestamp)
{
	for (auto &evt : m_events)
	{
		if (evt.id == old_id && evt.timestamp == old_timestamp)
		{
			evt.id = new_id;
			evt.timestamp = new_timestamp;

			return m_events;
		}
	}
	return m_events;
}

/**
 * @brief add new events to the class
 * 
 * @return std::vector<Event> 
 */
std::vector<Event> EventFilter::add_events(const std::vector<Event> &new_events)
{
	for (const auto &evt : new_events)
	{
		add_events(evt);
	}

	return m_events;
}

std::vector<Event> EventFilter::add_events(const Event &new_event)
{
	return add_events(new_event.id, new_event.timestamp);
}

std::vector<Event> EventFilter::add_events(const uint64_t &new_id, const uint32_t &new_timestamp)
{
	Event new_evt;
	new_evt.id = new_id;
	new_evt.timestamp = new_timestamp;

	m_events.push_back(new_evt);
	return m_events;
}

/**
 * @brief Remove events 
 * 
 * @return std::vector<Event> 
 */
std::vector<Event> EventFilter::remove_events(const std::vector<Event> &events_to_rm)
{
	for (auto &evt : events_to_rm)
	{
		remove_events(evt.id);
	}
	return m_events;
}

std::vector<Event> EventFilter::remove_events(const Event &event_to_rm)
{
	return remove_events(event_to_rm.id);
}

/**
 * TODO: No exception set for no existence
 */
std::vector<Event> EventFilter::remove_events(const uint64_t &id_to_rm, const uint32_t &ts_to_rm)
{
	auto it = m_events.begin();
	while (it != m_events.end())
	{
		if (it->id == id_to_rm && ((ts_to_rm == 0) || (ts_to_rm == it->timestamp)))
		{
			it = m_events.erase(it);
			continue;
		}
		++it;
	}
	return m_events;
}

/**
 * @brief Get the target events.
 * 
 * @param res_events Target events collection
 * @param id_to_filter target event id
 */
std::vector<Event> EventFilter::event_filter(const std::vector<Event> &events, const uint64_t &id_to_filter)
{
	print_thread_id();
	std::vector<Event> res_events;
	for (const auto &evt : events)
	{
		if (evt.id == id_to_filter)
		{
			res_events.push_back(evt);
		}
	}

	return res_events;
}

/**
 * @brief A proxy of EventFilter::event_filter to run in a thread
 * 
 * @param events 
 * @param id_to_filter 
 * @return std::vector<Event> 
 */
std::vector<Event> EventFilter::event_filter_thread(const std::vector<Event> &events, const uint64_t &id_to_filter)
{
	std::future<std::vector<Event>> ef_t =
		std::async(&EventFilter::event_filter, this,  events, id_to_filter);

	auto res_events = ef_t.get();

	return res_events;
}

void EventFilter::clear()
{
	m_events.clear();
}

std::vector<Event> EventFilter::get_m_events()
{
	return m_events;
}

uint64_t EventFilter::get_cnt() const
{
	return m_events.size();
}

void EventFilter::print_thread_id()
{
	std::thread::id tid = std::this_thread::get_id();
	std::cout << "Function runs in thread_id= " << tid << std::endl
			  << std::endl;
}