//
// Created by xlindo on 2020/5/9.
//

#ifndef SYNSENSEEVENTFILTER_EVENTFILTER_H
#define SYNSENSEEVENTFILTER_EVENTFILTER_H

#include <cstdint>
#include <vector>
#include <iostream>
#include <queue>
#include <thread>
#include <future>

struct Event
{
	uint64_t id = 0;
	uint32_t timestamp = 0;
};

class EventFilter
{
public:
	explicit EventFilter(const Event &evt);
	explicit EventFilter(const std::vector<Event> &events);
	// if the input is a queue of events
	explicit EventFilter(std::queue<Event> events);

	~EventFilter() = default;

	//  3 tasks:
	//  1. Transform event IDs and timestamps.
	//  2. Add events to the collection.
	//  3. Remove events to the collection based on a predicate.

	std::vector<Event> transform_event(const uint64_t &old_id, const uint32_t &old_timestamp, const uint64_t &new_id, const uint32_t &new_timestamp);

	std::vector<Event> add_events(const std::vector<Event> &new_events);
	std::vector<Event> add_events(const Event &new_event);
	std::vector<Event> add_events(const uint64_t &new_id, const uint32_t &new_timestamp);

	std::vector<Event> remove_events(const std::vector<Event> &events_to_rm);
	std::vector<Event> remove_events(const Event &event_to_rm);
	std::vector<Event> remove_events(const uint64_t &id_to_rm, const uint32_t &ts_to_rm = 0);

	std::vector<Event> event_filter(const std::vector<Event> &events, const uint64_t &id_to_filter);
	std::vector<Event> event_filter_thread(const std::vector<Event> &events, const uint64_t &id_to_filter);
	// void event_filter_thread(std::vector<Event> &res_events, const uint64_t &id_to_filter);

	// void* event_filter_proxy(void *);
	// void launch();

	void clear();
	std::vector<Event> get_m_events();
	void print_thread_id();

	uint64_t get_cnt() const;

private:
	std::vector<Event> m_events;
};

#endif //SYNSENSEEVENTFILTER_EVENTFILTER_H
