/**
 * @file EventFilterProxyMain.cpp
 * @author Xinlin DU (hi@xlindo.com)
 * 
 * @brief A simple test for the EventFilter.h
 * 
 * The compilation command could be 
 *      g++ EventFilterProxyMain.cpp EventFilter.h -o EventFilterProxyMain -std=c++11 -pthread
 * 
 * @version 0.1
 * @date 2020-05-12
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include <thread>
#include <iostream>
#include <string>
#include <algorithm>
#include <queue>
#include <variant>
#include <functional>
#include <map>

#include "EventFilter.h"

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;

void print_thread_id(const string &func_name)
{
    std::thread::id tid = std::this_thread::get_id();
    std::cout << "Function " << func_name << " runs in thread_id= " << tid << std::endl
              << std::endl;
}

void print_contents(const vector<Event> &events)
{
    cout << "========= Contents =========" << endl;
    for (const auto &evt : events)
    {
        cout << evt.id << " " << evt.timestamp << endl;
    }
    cout << "========= End =========" << endl<< endl;
}


vector<Event> remove_id_n(const vector<Event> &events, const std::vector<uint64_t> &id_rm)
{

    print_thread_id(__FUNCTION__);

    vector<Event> res_events;

    auto iter = events.cbegin();
    while (iter != events.cend())
    {
        if (id_rm.cend() == std::find(id_rm.cbegin(), id_rm.cend(), iter->id))
        {
            res_events.push_back(*iter);
        }
        ++iter;
    }

    return res_events;
}


vector<Event> add_event(const vector<Event> &events, const vector<Event> &new_events)
{

    print_thread_id(__FUNCTION__);

    vector<Event> res_events(events);
    res_events.insert(res_events.end(), new_events.begin(), new_events.end());

    return res_events;
}

int main()
{
    // Build a collections of vector
    vector<Event> three_evts, two_evts;
    Event evt_1, evt_2, evt_3, evt_4, evt_5;
    evt_1.id = 1;
    evt_1.timestamp = 5070001;
    evt_2.id = 2;
    evt_2.timestamp = 5100002;
    evt_3.id = 3;
    evt_3.timestamp = 5100003;
    three_evts.push_back(evt_1);
    three_evts.push_back(evt_2);
    three_evts.push_back(evt_3);

    evt_4.id = 4;
    evt_4.timestamp = 5100004;
    evt_5.id = 5;
    evt_5.timestamp = 5100005;
    two_evts.push_back(evt_4);
    two_evts.push_back(evt_5);


    // Test
    vector<Event> res_events;

    // add evt
    res_events = event_filter_T(three_evts, add_event, {evt_4});
    res_events = event_filter_thread_T(three_evts, add_event, {evt_4});

    // remove evt
    res_events = event_filter_T(res_events, remove_id_n, {3});
    res_events = event_filter_thread_T(res_events, remove_id_n, {3});


    // When the input is std::queue , deal with it in chain
    //
    // The target operations will be like 
    //      [remove_id_n, add_event, remove_id_n, add_event]
    //      [{1,2}, {evt_3}, {4}, {evt_5}]
    // so the final result will be like: 3, 3, 5, 5

    // Build test data, this can also be down by std::bind
    // input events
    vector<Event> in_q_e{three_evts}, res_q;
    in_q_e = event_filter_T(in_q_e, add_event, {two_evts});

    // input functions
    // there are two functions


    map<string, std::variant< std::function<vector<Event>(const vector<Event> &, const std::vector<uint64_t> &)>, std::function<vector<Event>(const vector<Event> &, const vector<Event> &)> >> map_funcs;
    
    
    std::queue< std::variant< std::function<vector<Event>(const vector<Event> &, const std::vector<uint64_t> &)>, std::function<vector<Event>(const vector<Event> &, const vector<Event> &)> > > in_q_func;
    in_q_func.push(remove_id_n);
    in_q_func.push(add_event);
    in_q_func.push(remove_id_n);
    in_q_func.push(add_event);

    // input args
    std::queue< std::variant< vector<uint64_t>, vector<Event> > > in_q_args;
    // std::queue<vector<uint64_t>> in_q_args;
    vector<uint64_t> args_1{1, 2};
    in_q_args.push(args_1);
    vector<Event> args_2{evt_3};
    in_q_args.push(args_2);
    vector<uint64_t> args_3{4};
    in_q_args.push(args_3);
    vector<Event> args_4{evt_5};
    in_q_args.push(args_4);


    // run the functions on a target events in sequence
     event_filter_q_T(in_q_e, in_q_func, in_q_args);

   

    return 0;
}