/**
 * @file EventFilterInFuncAllInOne.cpp
 * @author Xinlin DU (hi@xlindo.com)
 * @brief This file implements some functions of an event filter system based on the requirments:
 * 1. Transform event IDs and timestamps.
 * 2. Add events to the collection.
 * 3. Remove events to the collection based on a predicate.
 * 4. Filter the original input events directly with a determined ID
 * 
 * Some tests around are also given in the main function. 
 * But there is also something that not being considered yet.
 * 1. Thread locker, since there will be no data competition while correct input is given.
 * 2. Debug operations are not removed
 * 
 * The compilation command could be 
 *  
 *  g++ EventFilterInFuncAllInOne.cpp -o EventFilterInFuncAllInOne -std=c++11 -pthread
 * 
 * @version 0.1
 * @date 2020-05-10
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <cstdio>
#include <cstdint>
#include <thread>
#include <queue>
#include <vector>
#include <iostream>

using std::cout;
using std::endl;
using std::thread;
using std::vector;

struct Event
{
    uint64_t id = 0;
    uint32_t timestamp = 0;
};

/**
 * @brief 1. Transform event IDs and timestamps. 
 * 
 * @return vector<Event> 
 */
vector<Event> transform_event(vector<Event> &events, const uint64_t &old_id, const uint32_t &old_timestamp, const uint64_t &new_id, const uint32_t &new_timestamp)
{
    for (auto &evt : events)
    {
        if (evt.id == old_id && evt.timestamp == old_timestamp)
        {
            evt.id = new_id;
            evt.timestamp = new_timestamp;
            return events;
        }
    }
    return events;
}

/**
 * @brief 2. Add events to the collection.
 * 
 * @return vector<Event> 
 */
vector<Event> add_events(vector<Event> &events, const vector<Event> &new_events)
{
    events.insert(events.end(), new_events.cbegin(), new_events.cend());
    return events;
}

vector<Event> add_events(vector<Event> &events, const Event &evt)
{
    events.push_back(evt);
    return events;
}

vector<Event> add_events(vector<Event> &events, const uint64_t &new_id, const uint32_t &new_ts)
{
    Event evt;
    evt.id = new_id;
    evt.timestamp = new_ts;
    events.push_back(evt);
    return events;
}

/**
 * @brief 3. Remove events to the collection based on a predicate.
 * Assume the timestamp is distinct.
 * 
 * @return vector<Event> 
 */

vector<Event> remove_events(vector<Event> &events, const uint32_t &id_to_remove)
{
    auto iter = events.begin();
    while (iter != events.end())
    {
        if (iter->id == id_to_remove)
        {
            iter = events.erase(iter);
            continue;
        }
        ++iter;
    }
    return events;
}

/**
 * @brief Filter the current events vector with determined event id.
 * 
 * @param events 
 * @param target_id 
 * @return vector<Event> 
 */
vector<Event> event_filter(const vector<Event> &events, const uint64_t &target_id)
{
    vector<Event> res_events;
    for (const auto &evt : events)
    {
        if (evt.id == target_id)
        {
            res_events.push_back(evt);
        }
    }

    return res_events;
}

vector<Event> events_filter(const vector<Event> &events, const vector<uint64_t> &target_ids)
{
    vector<Event> res_events;
    vector<Event> tmp_events;
    for (const auto &t_id : target_ids)
    {
        tmp_events = event_filter(events, t_id);
        res_events.insert(res_events.end(), tmp_events.begin(), tmp_events.end());
    }

    return res_events;
}

// thread func_thread_proxy()

/**
 * @brief For test, print the contents of current events.
 */
void print_events(const vector<Event> &test_events)
{
    cout << "==========DEBUG CONTENTS===========" << endl;
    for (const auto &evt : test_events)
    {
        cout << evt.id << " " << evt.timestamp << endl;
    }
    cout << endl;
}

int main()
{
    // Test 1: for functions

    // build events vector
    vector<Event> test_events;
    Event evt_1, evt_2, evt_3;
    evt_1.id = 111;
    evt_1.timestamp = 5100001;
    evt_2.id = 111;
    evt_2.timestamp = 5100002;
    evt_3.id = 222;
    evt_3.timestamp = 5100003;
    test_events.push_back(evt_1);
    test_events.push_back(evt_2);
    test_events.push_back(evt_3);

    // modify events
    transform_event(test_events, 111, 5100002, 222, 5100002);
    transform_event(test_events, 222, 5100003, 111, 5100003);
    print_events(test_events);

    // add new event
    add_events(test_events, 333, 5100004);
    add_events(test_events, 444, 5100005);
    print_events(test_events);

    // remove event with id
    remove_events(test_events, 222);
    print_events(test_events);

    // filter event
    print_events(event_filter(test_events, 111));

    // filter events with the input as an output by other function
    vector<uint64_t> t_ids{111, 444};
    print_events(events_filter(remove_events(test_events, 111), t_ids));

    // Test 2: for multi-threads

    // build events vector
    test_events.clear();
    test_events.push_back(evt_1);
    test_events.push_back(evt_2);
    test_events.push_back(evt_3);
    print_events(test_events);

    // process in threads
    vector<thread> ef_pool;
    vector<Event> events_without_id_111 = test_events;
    vector<Event> events_without_id_444 = test_events;

    // run functions in separate threads, no locker set yet
    ef_pool.emplace_back(thread(remove_events, std::ref(events_without_id_111), 111));
    ef_pool.emplace_back(thread(remove_events, std::ref(events_without_id_444), 444));

    for (auto &ef : ef_pool)
    {
        ef.join();
    }

    print_events(events_without_id_111);
    print_events(events_without_id_444);

    return 0;
}