/**
 * @file EventFilter.h
 * @author Xinlin DU (hi@xlindo.com)
 * @brief Implement some functions for event filter functions.
 * 
 * @version 0.1
 * @date 2020-05-12
 * 
 * @TODO:
 *  1. parameters list of input
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <vector>
#include <cstdint>
#include <thread>
#include <future>
#include <iostream>
#include <queue>
#include <variant>
#include <functional>
#include <tuple>

struct Event
{
    uint64_t id = 0;
    uint32_t timestamp = 0;
};

template <typename T>
std::vector<Event> event_filter_T(const std::vector<Event> &orig_events, std::vector<Event> (*func)(const std::vector<Event> &, const T &), const T &args)
{
    // std::vector<Event>
    return func(orig_events, args);
}

/**
 * @brief Run the tasks in queue locally
 * 
 * @tparam T1 
 * @tparam T2 
 * @param orig_events 
 * @param funcs 
 * @param args 
 * @return std::vector<Event> 
 */
template <typename T1, typename T2>
std::vector<Event> event_filter_q_T(std::vector<Event> &orig_events, std::queue<T1> &funcs, std::queue<T2> &args)
{
    // std::queue<vector<Event>>
    // assume every func has arg(s)
    while ((!args.empty()) && (!funcs.empty()))
    {
        auto func = funcs.front();
        funcs.pop();
        auto arg = args.front();
        auto a = std::get<0>(arg);
        args.pop();

        // orig_events = func(orig_events, arg);
    }

    return orig_events;
}

/**
 * @brief run the task in a new thread
 * 
 * @tparam T 
 * @param orig_events 
 * @param func 
 * @param args 
 * @return std::vector<Event> 
 */
template <typename T>
std::vector<Event> event_filter_thread_T(const std::vector<Event> &orig_events, std::vector<Event> (*func)(const std::vector<Event> &, const T &), const T &args)
{
    std::future<std::vector<Event>> ef_t = std::async(std::launch::async, func, orig_events, args);
    return ef_t.get();
}